"""

Make a collection of ini files randomly sampled from the 
allowed range and run on them

"""

from cosmosis.runtime.parameter import Parameter
import os

values_file = "values.ini"
parameters = Parameter.load_parameters(values_file)

def write(parameters, values, filename):
	if os.path.exists(filename):
		raise ValueError("File {} already exists".format(filename))
	f = open(filename,'w')
	sec = None
	for p,v in zip(parameters, values):
		s = p.section
		n = p.name
		if s!=sec:
			f.write("[{}]\n".format(s))
			sec=s
		f.write('{} = {}\n'.format(n, v))
	f.close()


#mode 1 - only cosmological parameters sampled
def realize(varied_sections, name, n_realization):
	for i in xrange(1,n_realization+1):
		values = [p.random_point() 
				  if p.section in varied_sections
				  else p.start 
				  for p in parameters ]
		filename = 'secret_inputs/{}_{}.ini'.format(name, i)
		write(parameters, values, filename)

		datavector = 'datavectors/{}_{}.txt'.format(name, i)
		cmd = "cosmosis params.ini  -p extract.outfile={} pipeline.values={}".format(datavector, filename)
		print cmd
		status = os.system(cmd)
		if status:
			raise RuntimeError("Failed to run cosmosis on {}".format(filename))

N=5
cosmo='cosmological_parameters'
shear = 'shear_calibration_parameters'
photoz = 'wl_photoz_errors'

realize([cosmo],              'cosmo',              N)
realize([cosmo,shear],        'cosmo_shear',        N)
realize([cosmo,photoz],       'cosmo_photoz',       N)
realize([cosmo,shear,photoz], 'cosmo_shear_photoz', N)

