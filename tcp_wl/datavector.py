import numpy as np
import scipy.interpolate
import tabulate
from cosmosis.datablock import names, option_section

def execute(block, config):
	xiplus = {}
	ximinus = {}
	outfile,theta_sample = config
	shear_xi = names.shear_xi
	theta = block[shear_xi, 'theta'] #radians
	theta = np.degrees(theta)*60.0 # in arcmin
	for i in xrange(1,4):
		for j in xrange(1,4):
			if j>i:continue
			xip = block[shear_xi, 'xiplus_%d_%d'%(i,j)]
			xim = block[shear_xi, 'ximinus_%d_%d'%(i,j)]
			xiplus[(j,i)] = scipy.interpolate.interp1d(theta,xip,'cubic')
			ximinus[(j,i)] = scipy.interpolate.interp1d(theta,xim,'cubic')
	
	table = []
	for name,xi in zip(["xi+","xi-"], [xiplus, ximinus]):
		for i in xrange(1,4):
			for j in xrange(i,4):
				for t in theta_sample:
					x = xi[(i,j)](t)
					row = name, i, j, t, x
					table.append(row)
					#print row
	output = tabulate.tabulate(table, floatfmt="e", tablefmt='plain',
		headers=["#Type","i","j","theta","value"])
	open(outfile,'w').write(output)
	return 0

def setup(options):
	filename = options[option_section, "outfile"]
	theta_edges = np.logspace(np.log10(2.0), np.log10(300.0), 7, endpoint=True)
	theta_min = theta_edges[:-1]
	theta_max = theta_edges[1:]
	theta_sample = 2.0/3.0 * (theta_max**3 - theta_min**3)/(theta_max**2-theta_min**2)
	return [filename, theta_sample]