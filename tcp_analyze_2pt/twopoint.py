from astropy.io import fits
from astropy.table import Table
from enum34 import Enum
import os
import numpy as np

#FITS header keyword indicating 2-point data
TWOPOINT_SENTINEL = "2PTDATA"

class Types(Enum):
    galaxy_position_fourier = "GPF"
    galaxy_shear_emode_fourier = "GEF"
    galaxy_shear_bmode_fourier = "GBR"
    galaxy_position_real = "GPR"
    galaxy_shear_plus_real = "G+R"
    galaxy_shear_minus_real = "G-R"

    @classmethod
    def lookup(cls, value):
        for T in cls:
            if T.value==value:
                return T

def load_type_table():
    dirname = os.path.split(__file__)[0]
    table_name = os.path.join(dirname, "type_table.txt")
    type_table = Table.read(table_name, format="ascii")
    table = {}
    for (type1, type2, section, x, y) in type_table:
        table[(type1,type2)] = (section, x, y)
    return table

type_table = load_type_table()


class NumberDensity(object):
    def __init__(self, index, zlow, z, zhigh, nz):
        self.index = index
        self.zlow = zlow
        self.z = z
        self.zhigh = zhigh
        self.nz = nz

class SpectrumMeasurement(object):
    def __init__(self, name, bins, types, kernels, windows, angular_bin, value, angle=None, error=None):
        self.name = name
        self.bin1, self.bin2 = bins
        self.type1, self.type2 = types
        self.kernel1, self.kernel2 = kernels
        self.angular_bin = angular_bin
        self.angle = angle
        self.value = value
        self.windows = windows
        self.error = error

    def mask(self, mask):
        self.bin1 = self.bin1[mask]
        self.bin2 = self.bin2[mask]
        self.angular_bin = self.angular_bin[mask]
        self.angle = self.angle[mask]
        self.value = self.value[mask]

    def get_pair(self, bin1, bin2):
        w = (self.bin1==bin1) & (self.bin2==bin2)
        return self.angle[w], self.value[w]

    def get_error(self, bin1, bin2):
        if self.error is None:
            return None
        w = (self.bin1==bin1) & (self.bin2==bin2)
        return self.error[w]

    def theory_names(self):
        section, x_name, y_name = type_table[(self.type1.name,self.type2.name)]
        return section, x_name, y_name


class TwoPointFile(object):
    def __init__(self, spectra, kernels, windows, covmat):
        if windows is None:
            windows = {}
        self.spectra = spectra
        self.kernels = kernels
        self.windows = windows
        self.covmat = covmat

    def mask_bad(self, bad_value):
        "Go through all the spectra masking out data where they are equal to bad_value"
        masks = []
        #go through the spectra and covmat, masking out the bad values.
        for spectrum in self.spectra:
            #nb this will not work for NaN!
            mask = (spectrum.value != bad_value) 
            spectrum.mask(mask)
            print "Masking {} values in {}".format(mask.size-mask.sum(), spectrum.name)
            #record the mask vector as we will need it to mask the covmat
            masks.append(mask)

        #Also cut down the covariance matrix accordingly
        mask = np.concatenate(masks)
        self.covmat = self.covmat[mask, :][:, mask]

    def choose_data_sets(self, data_sets):
        """Strip out any data sets not in the given list."""
        data_sets = [d.lower() for d in data_sets]
        mask = []
        use = []
        for spectrum in self.spectra:
            if spectrum.name.lower() in data_sets:
                use.append(True)
                mask.append(np.ones(spectrum.bin1.size, dtype=bool))
            else:
                use.append(False)
                mask.append(np.zeros(spectrum.bin1.size, dtype=bool))
        for data_set in data_sets:
            if not any(spectrum.name.lower()==data_set for spectrum in self.spectra):
                raise ValueError("Data set called {} not found in two-point data file.".format(data_set))
        self.spectra = [s for (u,s) in zip(use,self.spectra) if u]
        mask = np.concatenate(mask)
        self.covmat = self.covmat[mask,:][:,mask]

    @classmethod
    def load(cls, filename, covmat_name="COVMAT"):
        fitsfile = fits.open(filename)
        spectra = []
        kernels = {}
        windows = {}

        #Load the covariance matrix from the file, typically stored as
        #image data.
        #It turns out it is more conventient to load this first
        covmat = fitsfile[covmat_name].data
        covmat_info = [0, covmat]


        #First load all the spectra in the file
        #Spectra are indicated by the "2PTDATA" keyword
        #in the header being "T"
        for extension in fitsfile:
            if extension.header.get(TWOPOINT_SENTINEL):
                spectra.append(cls._load_2point(extension, covmat_info))

        #Each spectrum needs kernels, usually some n(z).
        #These were read from headers in _load_2point above.
        #Now we are loading those kernels into a dictionary
        for spectrum in spectra:
            for kernel in (spectrum.kernel1, spectrum.kernel2):
                if kernel not in kernels:
                    kernels[kernel] = cls._load_kernel(fitsfile[kernel])

        #We might also require window functions W(ell) or W(theta). It's also possible
        #that we just use a single sample value of ell or theta instead.
        #If the spectra required it (according to the header) then we also
        #need to load those in.
        for spectrum in spectra:
            if spectrum.windows!="SAMPLE" and spectrum.windows not in windows:
                windows[spectrum.windows] = cls._load_windows(fitsfile[windows])

        #return a new TwoPointFile object with the data we have just loaded
        return cls(spectra, kernels, windows, covmat)

    @classmethod
    def _load_windows(cls, extension):
        raise NotImplementedError("non-sample window functions in ell/theta")


    @classmethod
    def _load_2point(cls, extension, covmat_info=None):
        name=extension.name
        #determine the type of the quantity involved
        type1 = Types.lookup(extension.header['QUANT1'])
        type2 = Types.lookup(extension.header['QUANT2'])

        #and the name of the kernels and the window function
        #extensions
        kernel1 = extension.header['KERNEL_1']
        kernel2 = extension.header['KERNEL_2']
        windows = extension.header['WINDOWS']

        if windows != 'SAMPLE':
            raise NotImplementedError("Have not yet coded window functions for angular bins")

        #Now load the data
        data = extension.data
        bin1 = data['BIN1']
        bin2 = data['BIN2']
        angular_bin = data['ANGBIN']
        value = data['VALUE']
        angle = data['ANG'] if 'ANG' in data.names else None

        #Load a chunk of the covariance matrix too if present.
        if covmat_info is None:
            error = None
        else:
            start_index, covmat = covmat_info
            count = len(bin1)
            error = covmat.diagonal()[start_index:start_index+count]**0.5
            covmat_info[0] = start_index + count


        return SpectrumMeasurement(name, (bin1, bin2), (type1, type2), (kernel1, kernel2), windows,
            angular_bin, value, angle, error)

    @classmethod
    def _load_kernel(cls, extension):
        #load in the n(z) kernel
        data = extension.data

        z = data['Z_MID']
        zlow = data['Z_LOW']
        zhigh = data['Z_HIGH']
        i = 1
        name = 'BIN{}'.format(i)
        kernels = []
        while name in data.names:
            nz = data[name]
            N = NumberDensity(i, zlow, z, zhigh, nz)
            kernels.append(N)
            i+=1 
            name = 'BIN{}'.format(i)
        return kernels


