

def setup(options):
    return []

def execute(block, options):
    bias = [1.35, 1.5, 1.65]
    for pos_bin in xrange(3):
        for shear_bin in xrange(5):
            name = "bin_{}_{}".format(shear_bin+1, pos_bin+1)
            print "Scaling GGL {} by {}".format(name, bias[pos_bin])
            block["shear_galaxy_cl", name] *= bias[pos_bin]
        name = "bin_{}_{}".format(pos_bin+1, pos_bin+1)
        print "Scaling Pos-Pos {} by {}".format(name, bias[pos_bin]**2)
        block['galaxy_cl', name] *= bias[pos_bin]**2