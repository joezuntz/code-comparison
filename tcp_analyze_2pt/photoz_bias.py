from cosmosis.datablock import option_section, names
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz
import scipy.ndimage
import numpy as np
import scipy.stats

def widen(z, p, sigma):
    dz = z[1] - z[0]
    print "GAUSSIAN THING", p.shape, sigma/dz
    q = scipy.ndimage.gaussian_filter1d(p, sigma/dz)
    return q	

def setup(options):
	sample = options.get_string(option_section, "sample", default="").strip()
	interpolation_order = options.get_string(option_section, "interpolation_order", default="cubic")
	print
	if sample == "":
		print "No particular sample specified for the photo-z bias code."
		print "I will just use the generic one: ", names.wl_number_density
		print "And look for errors in wl_photoz_errors"
	else:
		print "Sample set to '{0}' for photo-z bias code.".format(sample)
		print "Will look for n(z) numbers in nz_{0}.".format(sample)
		print "and for error parameters in nz_{0}_errors".format(sample)
	print
	return {"sample":sample, "order":interpolation_order}


def mean_z(z, nz):
	return (z*nz).sum()*(z[1]-z[0])

def execute(block, config):
	#Names of things
	sample = config['sample']
	order = config['order']

	#If we are using a named sample, look in 
	#the data block sections for that name
	if sample:
		pz = "nz_" + sample
		biases = pz + "_errors"
	#Otherwise just look in the general sections.
	else:
		pz = names.wl_number_density
		biases = "wl_photoz_errors"

	#Get the basic photo-z data
	nbin = block[pz, "nbin"]
	z = block[pz, "z"]

	#Loop through the bins
	for i in xrange(1,nbin+1):
		#Get the n(z) to work on for this bin
		bin_name = "bin_%d" % i

		#get the bias parameters
		bias = block[biases, "bias_%d"%i]
		sigma = block[biases, "sigma_%d"%i]
		#early exit if they are both zero
		if (bias==0 and sigma==0):
			continue

		#Load and normalize the n(z)
		nz = block[pz, bin_name]
		nz/=np.trapz(nz,z)

		#We can't use a convolution, because we can't make it thinner like that.
		#Instead we spread things out from the mean and add a bias using:
		# f'(z) = f((1+sigma)*(z-mz)+mz-bias)
		#We use a spline to do this, since this is quite flexible
		mz = mean_z(z, nz)

		#Make a spline of n(z) and evaluate it at the alternative points
		f = interp1d(z, nz, kind=order, fill_value = 0.0, bounds_error=False)
		nz = f((1+sigma)*(z-mz)+mz-bias)

		#renormalize: some probability may have slipped off the end of the range
		nz/=np.trapz(nz,z)

		#Save the new debiased n(z) back to the block
		block[pz, bin_name] = nz
	return 0

def cleanup(config):
	pass
