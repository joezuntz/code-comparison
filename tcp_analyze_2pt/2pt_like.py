from cosmosis.gaussian_likelihood import GaussianLikelihood
from astropy.io import fits
from scipy.interpolate import interp1d
import numpy as np
import twopoint



class TwoPointLikelihood(GaussianLikelihood):
	like_name = "2pt"
	
	def build_data(self):
		filename = self.options.get_string('data_file')

		self.do_plot = self.options.get_bool('do_plot', default=False)

		self.two_point_data = twopoint.TwoPointFile.load(filename)
		if self.options.get_bool("cut_zeros", default=False):
			print "Removing 2-point values with value=0.0"
			self.two_point_data.mask_bad(0.0)

		all_names = [spectrum.name for spectrum in self.two_point_data.spectra]

		#Optionally strip out some data sets
		data_sets = self.options.get_string("data_sets", default="all")
		if data_sets!="all":
			data_sets = data_sets.split()
			self.two_point_data.choose_data_sets(data_sets)

		used_names = [spectrum.name for spectrum in self.two_point_data.spectra]

		print "Found these data sets in the file:"
		for name in all_names:
			if name in used_names:
				print "    - ",name, "  [using in likelihood]"
			else:
				print "    - ",name, "  [not using in likelihood]"

		#build up the data vector from all the separate vectors.
		#TODO: options to choose which spectra to include
		data_vector = np.concatenate([spectrum.value for spectrum in self.two_point_data.spectra])

		if len(data_vector)==0:
			raise ValueError("No values left in 2-point data vector")

		#The x data is not especially useful here, so return None.
		return None, data_vector


	def build_covariance(self):
		C = np.array(self.two_point_data.covmat)
		return C

	def extract_theory_points(self, block):
		theory = []
		# We have a collection of data vectors, one for each spectrum
		# that we include. We concatenate them all into one long vector,
		# so we do the same for our theory data so that they match
		for spectrum in self.two_point_data.spectra:
			theory.append(self.extract_spectrum_prediction(block, spectrum))

		return np.concatenate(theory)

	def extract_spectrum_prediction(self, block, spectrum):
		#We may need theory predictions for multiple different
		#types of spectra: e.g. shear-shear, pos-pos, shear-pos.
		#So first we find out from the spectrum where in the data
		#block we expect to find these - mapping spectrum types
		#to block names
		section, x_name, y_name = spectrum.theory_names()

		#We need the angle (ell or theta depending on the spectrum)
		#for the theory spline points - we will be interpolating
		#between these to get the data points
		angle_theory = block[section, x_name]

		#Now loop through the data points that we have.
		#For each one we have a pairs of bins and an angular value.
		#This assumes that we can take a single sample point from
		#each theory vector rather than integrating with a window function
		#over the theory to get the data prediction - this will need updating soon.
		bin_data = {}
		theory_vector = []
		for (b1, b2, angle) in zip(spectrum.bin1,spectrum.bin2,spectrum.angle):
			#We are going to be making splines for each pair of values that we need.
			#We make splines of these and cache them so we don't re-make them for every
			#different theta/ell data point
			if (b1,b2) in bin_data:
				#either use the cached spline
				theory_spline = bin_data[(b1,b2)]
			else:
				#or make a new cache value
				#load from the data block and make a spline
				#and save it
				if block.has_value(section, y_name.format(b1,b2)):
					theory = block[section, y_name.format(b1,b2)]
				#It is okay to swap if the spectrum types are the same - symmetrical
				elif block.has_value(section, y_name.format(b2,b1)) and spectrum.type1==spectrum.type2:
					theory = block[section, y_name.format(b2,b1)]
				else:
					raise ValueError("Could not find theory prediction {} in section {}".format(section, y_name.format(b1,b2)))
				theory_spline = interp1d(angle_theory, theory)
				bin_data[(b1,b2)] = theory_spline

			#use our spline - interpolate to this ell or theta value
			#and add to our list
			theory = theory_spline(angle)
			theory_vector.append(theory)

		if self.do_plot:
			import pylab
			nbin = max(spectrum.bin1.max(), spectrum.bin2.max())
			for b1 in xrange(1,nbin+1):
				for b2 in xrange(1,nbin+1):
					if (b1,b2) not in bin_data:
						continue
					pylab.subplot(nbin, nbin, (b1-1)*nbin+b2)
					pylab.loglog(angle_theory, bin_data[(b1,b2)](angle_theory))
					xdata, ydata = spectrum.get_pair(b1, b2)
					yerr = spectrum.get_error(b1, b2)
					pylab.errorbar(xdata, ydata, yerr, fmt='+')
					pylab.xlim(xdata.min(), xdata.max())
					pylab.ylim(ydata.min(), ydata.max())
			pylab.savefig("cmp_{}.pdf".format(spectrum.name))
			pylab.close()


			# print b1, b2, nbin, (b1-1)*nbin+b2
			# print "angle = ", angle
			#
			
		#Return the whole collection as an array
		theory_vector = np.array(theory_vector)
		return theory_vector

setup, execute, cleanup = TwoPointLikelihood.build_module()
