import scipy.interpolate
from cosmosis.datablock import names, option_section
import numpy as np
"""
Extract the outputs for the TJP code comparison exercise.

a) Growth factor at z = 0,1,2,3,4,5
b) Comoving radial distance [Mpc/h] at z = 0,1,2,3,4,5
c) Linear matter power spectrum [(Mpc/h)^3] at z = 0,1,2,3; 1.e-3 h/Mpc <= k <= 10, 10 bins/decade
d) 

"""


def extract_growth(block):
	z_sample = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
	z = block['growth_parameters', 'z']
	d = block['growth_parameters', 'd_z']
	spline = scipy.interpolate.interp1d(z,d,'cubic')
	d_sample = spline(z_sample)
	block['code_comparison', 'growth'] = d_sample

def extract_comoving(block):
	z_sample = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
	z = block['distances', 'z'][::-1]
	d = block['distances', 'd_m'][::-1]
	spline = scipy.interpolate.interp1d(z,d,'cubic')
	d_sample = spline(z_sample)
	block['code_comparison', 'comoving_distance'] = d_sample

def extract_linear(block):
	k, z, p = block.get_grid("matter_power_lin", "k_h", "z", "p_k")
	spline = scipy.interpolate.interp2d(k, z, p.T, kind='cubic')
	k_sample = np.logspace(-3, 1, 41)
	z_sample = 0.0
	p_sample = spline(k_sample, z_sample)
	block['code_comparison', 'matter_power_lin'] = p_sample

def extract_sigma(block):
	z, R, sigma2 = 	block.get_grid("sigma_r", "z", "r", "sigma2")
	sigma_r = sigma2[0]**0.5
	print R, sigma_r
	block['code_comparison', 'sigma_r'] = sigma_r


EXTRACTIONS = [extract_growth, extract_comoving, extract_linear, extract_sigma]


def setup(options):
	fatal = options[option_section, "fatal_errors"]
	return {"fatal":fatal}

def execute(block, config):
	for function in EXTRACTIONS:
		try:
			function(block)
		except:
			if config['fatal']:
				raise
			print "Count not use ", function.__name__

	return 0